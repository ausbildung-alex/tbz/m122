# Meilenstein B

[TOC]

Wir mussten ein Script zu unseren Zielen schreiben. Hier wäre einmal der Link zur [Python](./script.py) und [Config](./config.cfg) Datei. Allenfalls finden sie unten noch den Code: 

## Python Code
Hier finden sie den Python Code:  

``` 
import requests
import pandas as pd
import configparser
import os
from datetime import datetime
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders

config = configparser.ConfigParser()
config.read('config.cfg')

LOCATIONS = config['LOCATIONS']['locations'].split(',')
SENDER_EMAIL = config['EMAIL']['sender']
EMAIL_PASSWORD = config['EMAIL']['password']
RECIPIENT_EMAIL = config['EMAIL']['recipient']
SMTP_SERVER = config['EMAIL']['smtp_server']
SMTP_PORT = int(config['EMAIL']['smtp_port'])

LAT_LON = {
    "Genf": [46.2044, 6.1432],
    "Bern": [46.9481, 7.4474],
    "Zuerich": [47.3769, 8.5417],
    "Tessin": [46.3317, 8.8005],
    "Graubuenden": [46.8508, 9.5322],
    "Basel": [47.5596, 7.5886]
}

def get_weather_data(lat, lon):
    url = f"https://api.open-meteo.com/v1/forecast?latitude={lat}&longitude={lon}&hourly=temperature_2m&start={datetime.now().strftime('%Y-%m-%dT00:00:00Z')}&end={datetime.now().strftime('%Y-%m-%dT23:59:59Z')}&timezone=Europe/Zurich"
    response = requests.get(url)
    data = response.json()
    return data

# Daten prozess
def process_weather_data(data):
    temperatures = data['hourly']['temperature_2m']
    avg_temp = sum(temperatures) / len(temperatures)
    return round(avg_temp, 1)

# Email funktion
def send_email(file_path):
    msg = MIMEMultipart()
    msg['From'] = SENDER_EMAIL
    msg['To'] = RECIPIENT_EMAIL
    msg['Subject'] = "Täglicher Wetterbericht"

    body = "Hier finden sie den täglichen Wetterbericht."
    msg.attach(MIMEText(body, 'plain'))

    attachment = open(file_path, "rb")
    part = MIMEBase('application', 'octet-stream')
    part.set_payload((attachment).read())
    encoders.encode_base64(part)
    part.add_header('Content-Disposition', "attachment; filename= %s" % os.path.basename(file_path))
    msg.attach(part)

    server = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
    server.starttls()
    server.login(SENDER_EMAIL, EMAIL_PASSWORD)
    text = msg.as_string()
    server.sendmail(SENDER_EMAIL, RECIPIENT_EMAIL, text)
    server.quit()


def main():
    weather_data = {}
    for location in LOCATIONS:
        lat, lon = LAT_LON[location]
        data = get_weather_data(lat, lon)
        avg_temp = process_weather_data(data)
        weather_data[location] = avg_temp

    df = pd.DataFrame(list(weather_data.items()), columns=['Ort', 'Durchschnittstemperatur'])
    file_path = f"weather_report_{datetime.now().strftime('%Y-%m-%d')}.csv"
    df.to_csv(file_path, index=False)

    send_email(file_path)
    os.remove(file_path)

    print(f"Wetter Report wurde erfolgreich geschickt {RECIPIENT_EMAIL}")


if __name__ == "__main__":
    main()
```

## Config 
Hier finden sie den Inhalt des Configs: 

```
[LOCATIONS]
locations = Genf,Bern,Zuerich,Tessin,Graubuenden,Basel

[EMAIL]
sender = wetterdev@gmail.com
password = dqkqxfyxorazcrni
recipient = alexander.wasser@edu.tbz.ch
smtp_server = smtp.gmail.com
smtp_port = 587
``` 