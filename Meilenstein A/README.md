## 2. Anforderungsdefinition (Meilenstein A)
### Wetteranalyse
 
#### Fachlicher Inhalt:
 Dieses Skript soll eine Statistik erstellen, die die durchschnittlichen Temperaturen von Genf, Bern, Zürich, Tessin und Graubünden vom heutigen Tag zeigt. Dies wir in einer Statistik dargestellt.
 
#### Setup und Automation:
Der Kundenserver / -dienst ist ein Weatherdaten API. Das Skript automatisiert den Prozess des Abrufens der historischen Temperaturdaten für die spezifizierten Kantone über die API.
 
#### Details:
| Aspekt                   | Beschreibung                                                 |
|--------------------------|--------------------------------------------------------------|
| Konfiguration (.cfg)     | `config.cfg`, in der der API-Schlüssel und die Kantone angegeben werden. Die Daten sollen jeden Tag um 09:00 in einer JSON Datei gesammelt werden. Anschliessend soll es in eine CSV Datei umgewandelt und geschickt werden. |
| Get-Prozedur (.raw)      | Verwendung von 'curl' für den Abruf der Daten. Wird als jason Datei widergegeben              |
| Verarbeitung (process)   | Analyse der Daten zur Darstellung in der Statistik. Verarbeitung in eine CSV Datei.         |
| Weiterreichung (.fmt)    | Speicherung der Daten in einer CSV-Datei.  Wird dann per EMail automatisch versendet                  |
| Sicherheitsaspekte       | Sicherheitsmaßnahmen zum Schutz sensibler Daten.             |

#### Komponenten 
[Bild](./Images/Komponenten%20(1).png)

#### UML Diagramm
[Bild](./Images/UML.png)